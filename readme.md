# How to run?
It should be enough to just clone and run the start.sh file with sudo, or as a root. It opens a local PHP server on address `127.0.0.1:80`.
### How to run manually
Just install PHP on your machine and then make sure that your `php` command works properly. Then enter the cloned folder and run:

```
php -S 127.0.0.1:80 -t www
```
Alternatively you can install any other webserver, like apache2 or nginx and point the root folder to `www`. As the last step, make sure the folders `log` and `temp` are accessible to anyone.

# How to analyse?
App has preconfigured PPStan.
Insall PHP `MBString` extension for it to work properly. Ten all you need to do is run in root direcotry of the project following command:
```
vendor/bin/phpstan analyse app
```
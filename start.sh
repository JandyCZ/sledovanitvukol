#!/bin/sh

if ! [ $(id -u) = 0 ]; then
   echo "Please, run as root. (sudo)"
   exit 1
fi
    
if php -v > /dev/null ; then
        php -S 127.0.0.1:80 -t www
else
        echo "PHP is not installed."
fi

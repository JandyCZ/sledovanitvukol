<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Nette\Forms\Form;
use Nette\Application\UI\Form as UIForm;

use Tracy\Debugger;

final class HomepagePresenter extends Nette\Application\UI\Presenter
{

	private Nette\Database\Explorer $database;

	private array $filterParams = [];

	public function __construct(Nette\Database\Explorer $database)
	{
		$this->database = $database;
	}

	public function renderDefault(): void
	{
		/*$this->template->channels = $this->database->table('Channels')
			->select("*, ChannelGroups.name AS gname")
			->where($this->filterParams)
			->fetchAssoc("id");
		*/ 
		//Maybe bugged?? Anyways, it was not working properly, decided to make a regular SQL query.

		$this->template->channels = $this->database->query("
			SELECT Channels.id, Channels.name, Channels.description, ChannelGroups.name AS gname FROM Channels
			LEFT JOIN ChannelGroups ON Channels.channelGroup = ChannelGroups.id
			WHERE ?
		", $this->filterParams);

	}

	protected function createComponentFilterForm(): UIForm
	{
		$channelGroups = $this->database->table('Channels')
			->select("*, channelGroup.name AS gname")
			->where("channelGroup IS NOT NULL")
			->fetchPairs("channelGroup", "gname");

		$form = new UIForm;
		$form->getElementPrototype()->addAttributes(["class" => "ajax"]);

		$form->addText('name', 'Name:');
		$form->addText('description', 'Description:');
		$form->addCheckboxList("group", "Channel Group", $channelGroups);
		$form->addSubmit('send', 'Search');

		$form->onSuccess[] = [$this, 'filterFormFiltered'];
		return $form;
	}

	public function filterFormFiltered(Form $form, $data): void
	{
		bdump($data);

		$this->filterParams["Channels.name LIKE"] = "%$data->name%";
		$this->filterParams["Channels.description LIKE"] = "%$data->description%";
		if (!empty($data->group)) $this->filterParams["Channels.channelGroup"] = $data->group;

		$this->flashMessage('Filtry byly aplikovány.');

		if ($this->isAjax()) {
			$this->redrawControl();
		}
	}

	protected function createComponentAddForm(): UIForm
	{
		$channelGroups = $this->database->table('ChannelGroups')
			->fetchPairs("id", "name");

		$form = new UIForm;
		$form->getElementPrototype()->addAttributes(["class" => "ajax"]);
		$form->addText('id', 'Slug (ID):')->setRequired();
		$form->addText('name', 'Name:')->setRequired();
		$form->addText('description', 'Description:');
		$form->addSelect("group", "Channel Group", $channelGroups)->setRequired();
		$form->addText('index', 'Index (Order):')->setRequired();
		$form->addSubmit('send', 'Add');

		$form->onSuccess[] = [$this, 'addFormFiltered'];
		return $form;
	}

	public function addFormFiltered(Form $form, $data): void
	{
		bdump($data);

		$this->database->table('Channels')->insert([
			'id' => $data->id,
			'name' => $data->name,
			'order' => $data->index,
			'channelGroup' => $data->group,
			'description' => $data->description
		]);

		$this->flashMessage("Položka byla přidána.");

		if ($this->isAjax()) {
			$this->redrawControl();
		} else {
			$this->redirect("this");
		}
	}

	public function handleDelete($id): void
	{
		bdump($id);
		$this->database->table("Channels")->where('id', $id)->delete();
		
		$this->flashMessage("Položka byla smazána.");

		if ($this->isAjax()) {
			$this->redrawControl();
		}
	}

}
